# Android App - Music Player

Built with [Android Jetpack](https://developer.android.com/jetpack) components:
*  Data Binding
*  LiveData
*  Navigation
*  Paging
*  ViewModel
*  and more

Architectural pattern: [MVVM](https://developer.android.com/jetpack/docs/guide#recommended-app-arch)

Backend: [Firebase](https://firebase.google.com/)

![alt](https://i.imgur.com/NEsAn5E.png)
